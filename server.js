var express  = require('express'),
    path     = require('path'),
    bodyParser = require('body-parser'),
    app = express(),
    expressValidator = require('express-validator');

var dateTime = require('node-datetime');

/*Set EJS template Engine*/
app.set('views','./views');
app.set('view engine','ejs');

app.use(express.static(path.join(__dirname, 'public')));
app.use(bodyParser.urlencoded({ extended: true })); //support x-www-form-urlencoded
app.use(bodyParser.json());
app.use(expressValidator());

/*MySql connection*/
var connection  = require('express-myconnection'),
    mysql = require('mysql');

app.use(

    connection(mysql,{
        host     : 'localhost',
        user     : 'root',
        password : '',
        database : 'fitness',
        debug    : false //set true if you wanna see debug logger
    },'request')

);

app.get('/',function(req,res){
    // var index = 46;
    // for(i = 227; i < 1202; i++) {
    //     var query = 'UPDATE exercises SET workout_id = ' + index + ' WHERE id = '+ i + ';'
    //     console.log(query);
    //     if((i-1) % 5 == 0)
    //         index++;
    // }
    res.send('Welcome');
});

//RESTful route
var router = express.Router();

//now we need to apply our router here
app.use('/', router);


/*------------------------------------------------------
*  This is router middleware,invoked everytime
*  we hit url /api and anything after /api
*  like /api/user , /api/user/7
*  we can use this for doing validation,authetication
*  for every route started with /api
--------------------------------------------------------*/
router.use(function(req, res, next) {
    console.log(req.method, req.url);
    next();
});

router.post('/login', function(req, res, next) {

    var data = {
        email: req.body.email,
        password: req.body.password
    }

    console.log('login param', data);

    req.getConnection(async function(err, conn) {
        if(err) return next("Cannot connect");
        
        try {
            var results = await Users.loginUser(conn, data);
            console.log('login result', results);
            res.send({
                status: 200,
                error: null,
                response: results
            });
        } catch(error) {
            console.log('login error', error);
            res.send({
                status: 500,
                error: 'Invalid email or password',
                response: null
            });
        }
    });
});

var users = router.route('/users');

users.get(function(req,res,next){

    req.getConnection(function(err,conn){

        if (err) return next("Cannot Connect");

        var query = conn.query('SELECT * FROM t_user',function(err,rows){

            if(err){
                console.log(err);
                return next("Mysql error, check your query");
            }

            res.render('user',{title:"RESTful Crud Example",data:rows});

         });

    });

});

//post data to DB | POST
users.post(function(req,res,next){  

    //validation
    // req.assert('name','Name is required').notEmpty();
    req.assert('email','A valid email is required').isEmail();
    req.assert('password','Enter a password 6 - 20').len(6,20);

    var errors = req.validationErrors();
    if(errors){
        res.status(422).json(errors);
        return;
    }

    var date = dateTime.create().format('Y-m-d'); // Y-m-d H:M:S

    //get data
    var userData = {
        email:req.body.email,
        password:req.body.password,
        tall: req.body.tall,
        weigh: req.body.weigh,
        created_at: date
     };

    var profileData = {
        goal: req.body.goal,
        level: req.body.level
    }
    
    //inserting into mysql
    req.getConnection(async function (err, conn){

        if (err) return next("Cannot Connect");

        try {
            var currentUserId = await Users.createUser(conn, userData, next);
            console.log('user id: ', currentUserId);

            try {
                var profileId = await Profile.getProfileId(conn, profileData);
                // console.log('profile id', profileId);

                try {
                    console.log('current user: ', currentUserId);
                    await Users.setProfileId(conn, currentUserId, profileId);
                    res.send({
                        status: 201,
                        error: null, 
                        response: {
                            userId: currentUserId,
                            profileId: profileId
                        }
                    });
                } catch(error) {
                    console.log('profile set error: ', error);
                    res.send({
                        status: 500, 
                        error: error,
                        response: null
                    })
                }

            } catch(error) {
                console.log('profile get error: ', error);
                res.send({
                    status: 500,
                    error: error,
                    response: null
                })
            }

        } catch(error) {
            console.log('user error: ', error);
            res.send({
                status: 400,
                error: error,
                response: null
            });
        }

     });

});

//get workouts and exercises
var workout = router.route('/workouts/:profileId');

workout.get(function (req, res, next) {

    var profileId = req.params.profileId;

    req.getConnection(async function (err, conn) {
        if(err) return next("Cannot connet! DB server is not activate.")

        try {
            const workouts = await Workouts.getWorkouts(profileId, conn);
            var workoutsArray = {};
            var key;
            for(let workout of workouts) {

                if(workout.id < 10) {
                    key = "workout0" + workout.id;
                } else {
                    key = "workout" + workout.id;
                }
                // var key = workout.id;
                try {
                    const exercises = await Exercises.getExercises(workout.id, conn);
                    workoutsArray[key] = exercises;
                } catch (error) {
                    console.log('get exercises error', error);
                    return next('No exercies found');
                }
                
            }

            res.send({
                status: 200,
                error: null,
                response: workoutsArray
            });

        } catch(error) {
            console.log('error', error);
            res.send({
                status: 500,
                error: error,
                response: null
            });
        }

    });

});

//record of workouts
var workoutRecord = router.route('/workoutRecords/:userId');

workoutRecord.get(function(req, res, next) {
    var userId = req.params.userId;

    req.getConnection(async function(error, conn) {
        if(error) {
            return next('Cannot connect');
        }

        try {

            const recordsData = await Records.getWorkoutsRecords(userId, conn, next);
            var exerciseIdArray = [];
            exerciseIdArray = recordsData;
            var exerciseArray = [];

            try {
                for(i = 0; i < exerciseIdArray.length; i++) {
                    const exercise = await Exercises.getExercise(exerciseIdArray[i].exercise_id, conn);
                    exerciseArray.push(exercise);
                }

                res.send({
                    status: 200, 
                    error: null, 
                    response: exerciseArray
                });

            } catch(error) {
                console.log(error);
                return next(error);

            }

        } catch(error) {
            console.log('Get exercise from record failed: ', error);
            res.send({
                status: 404,
                error: error,
                response: null
            })
        }
    });
});

workoutRecord.post(function(req, res, next) {
    var userId = req.params.userId;

    req.getConnection(async function(error, conn) {
        //validation

        // req.assert('name','Name is required').notEmpty();
        // req.assert('email','A valid email is required').isEmail();
        // req.assert('password','Enter a password 6 - 20').len(6,20);

        var errors = req.validationErrors();
        if(errors){
            res.status(422).json(errors);
            return;
        }

        var date = dateTime.create().format('Y-m-d');

        //get data
        var data = {
            user_id: userId,
            exercise_id: req.body.exerciseId,
            repeatition: 1,
            updated_at: date
        };

        try {

            const recordResult = await Records.recordExercise(data, conn);
            res.send({
                status: 200,
                error: null,
                response: {
                    recordResult
                }
            });

        } catch(error) {
            console.log('workouts recorded failed: ', error);
            res.send({
                status: 500, 
                error: error,
                response: null
            });
        }
    });
});

//get nutritions
var nutritions = router.route('/nutritions');
nutritions.get(function (req, res, next) {
    req.getConnection(async function (err, conn) {
        if(err) {
            return next('Cannot connect');
        }

        try {
            var nutritionData = await Nutritions.getNutritions(conn, next);
            res.send({
                status: 200,
                error: null,
                response: nutritionData
            });
        } catch(error) {
            console.log('get nutrition error', error);
            res.send({
                status: 500,
                error: error,
                response: null
            });
        }
    });
});

//record of nutritions
var recordNutrtion = router.route('/nutritionRecords/:userId');
recordNutrtion.post(function (req, res, next) {
    var userId = req.params.userId;
    var date = dateTime.create().format('Y-m-d');
    var data = {
        user_id: userId, 
        nutrition_day: req.body.nutritionDay,
        meal: req.body.meal,
        updated_at: date,
        repeatition: 1
    }

    req.getConnection(async function (err, conn) {
        if(err) {
            return next('Cannot connect');
        }

        try {
            var recordResult =  await Records.recordNutritions(data, conn, err);
            res.send({
                status: 200, 
                error: null,
                response: {
                    recordResult
                }
            });
        } catch(error) {
            console.log('nutrition record error: ', error);
            res.send({
                status: 500,
                error: error,
                response: null
            });
        }
    });
});

class Users {

    static createUser(conn, userData, next) {

        return new Promise(function (resolve, reject) {
            var query = conn.query("SELECT id FROM users WHERE email = ?", userData.email, (err, rows) => {
                if(err) {
                    console.log(error);
                    reject('Check your query');
                } else {
                    if(rows.length > 0) {
                        reject('Another user already used this email');
                    } else {
                        var query = conn.query("INSERT INTO users set ? ",userData, (err, rows) => {
                            if(err){
                                console.log(err);
                                reject(err);
                            } else {
                                console.log('insert id: ', rows.insertId);
                                resolve(rows.insertId);
                            }
                        });
                    }
                }
            });
        });
        
    }

    static loginUser(conn, data) {
        var email = data.email;
        var password = data.password;

        return new Promise(function (resolve, reject) {
            var query = conn.query("SELECT id as userId, profile_containers_id as profileId from users WHERE email = ? AND password = ?", [email, password], function(err, rows) {
                console.log('login query:', query.sql);

                if(err) {
                    console.log(err);
                    reject(err);
                } else if(rows.length < 1) {
                    reject('No user found');
                } else {
                    console.log('login result: ', rows);
                    resolve(rows[0]);
                }
            });
        });
    }

    static setProfileId(conn, userId, profileId) {

        return new Promise(function (resolve, reject) {
            var query = conn.query("UPDATE users set ? WHERE id = ? ",[{profile_containers_id: profileId}, userId], function(err, rows){
                console.log('query: ', query.sql);
                if(err){
                     console.log(err);
                     reject(err);
                } else {
                    resolve();
                }
            });
        });

    }

}

class Profile {

    static getProfileId(conn, profiledata) {

        return new Promise(function(resolve, reject) {
            var query = conn.query('SELECT id FROM profile_containers WHERE goal = ? AND level = ?', [profiledata.goal, profiledata.level], function(err,rows){
                if(err){
                    console.log(err);
                    reject(err);
                } else if(rows.length < 1) {
                    reject('No Records Found');
                } 
                else {
                    resolve(rows[0].id);
                }
             });
        });

    }
}

class Workouts {

    static getWorkouts(profileId, conn) {

        return new Promise( (resolve, reject) => {
            var query = conn.query("SELECT * FROM workouts WHERE profile_containers_id = ? ",[profileId], function (err,rows) {
                if(err){
                    console.log(err);
                    // return next("Mysql error, check your query");
                    reject(err);
                } else if(rows.length < 1) {
                    reject("Workouts not found");
                } else {
                    resolve(rows);
                }
            });
        });

    }
}

class Exercises {

    static getExercises(workoutsId, conn) {
        return new Promise(function (resolve, reject) {

            var query = conn.query("SELECT * FROM exercises WHERE workout_id = ?", [workoutsId], function (err, rows) {

                if(err) {
                    console.log(err);
                    reject(err);
                } else if(rows.length < 1) {
                    reject("Exercises Not Found");
                } else {
                    resolve(rows);
                }
            });

        });
    }

    static getExercise(exerciseId, conn) {
        return new Promise(function (resolve, reject) {
            var query = conn.query("SELECT * FROM exercises WHERE id = ?", [exerciseId], function(error, rows) {
                if(error) {
                    console.log(error);
                    reject(error);
                } else {
                    resolve(rows);
                }
            });
        });
    }
}

class Records {
    
    static recordExercise(data, conn) {

        return new Promise(function (resolve, reject) {
            var query = conn.query("SELECT id, repeatition FROM workouts_process WHERE user_id = ? AND exercise_id = ?", [data.user_id, data.exercise_id], function(err, rows) {
                if(err) {

                    console.log('get recorded exercise query error');
                    reject(err);

                } else if(rows.length < 1) {

                    console.log('record new exercise');
                    // Records.recordNewExercise(data, conn);

                    var query = conn.query("INSERT workouts_process set ?", [data], function(err, rows){
                        if(err){
                            console.log(err);
                            reject(err);
                        }else {
                            resolve(rows.insertId);
                        }
                    });

                } else {

                    console.log('update recorded exercise');
                    // Records.updateRecordedExercise();
                    var currentId = rows[0].id;
                    var repeatition = rows[0].repeatition + 1;
                    var query = conn.query("UPDATE workouts_process SET repeatition = ?, updated_at = ? WHERE id = ?", [repeatition, data.updated_at, currentId], function(err, rows) {
                        if(err) {
                            console.log(err);
                            reject(err);
                        } else {
                            console.log('update result: ', rows);
                            resolve('success');
                        }
                    });

                }
            });
        });
    }

    static recordNewExercise(data, conn) {
        console.log('record new exercise query');

        var query = conn.query("INSERT workouts_process set ?", [data], function(err, rows){
            if(err){
                console.log(err);
                reject(err);
            }else {
                resolve(rows.insertId);
            }
        });
    }

    static updateRecordedExercise() {
        console.log('update recorded exercise query');
    }

    static getWorkoutsRecords(userId, conn, next) {
        return new Promise(function (resolve, reject) {
            var query = conn.query("SELECT * FROM workouts_process WHERE user_id = ?", [userId], function(error, rows) {
                console.log('query', query.sql);
                if(error) {
                    console.log(error);
                    // return next('check your query');
                    reject('Check your query');
                }

                if(rows.length < 1) {
                    reject('no such data');
                } else {
                    resolve(rows);
                }
            });
        });
    }

    static recordNutritions(data, conn, error) {

        return new Promise(function (resolve, reject) {
            var query = conn.query("SELECT id, repeatition FROM nutritions_process WHERE user_id = ? AND nutrition_day = ? AND meal = ?", [data.user_id, data.nutrition_day, data.meal], function(error, rows) {
                if(error) {
                    console.log('query error', err);
                    reject(err);
                } else if(rows.length < 1) {
                    console.log('record new nutrition');
                    
                    var query = conn.query("INSERT nutritions_process set ?", data, function(error, rows) {
                        if(error) {
                            console.log(error);
                            reject(error);
                        } else {
                            console.log('record nutrition', rows);
                            resolve(rows);
                        }
                    });

                } else {
                    console.log('update recorded nutrition');
                    
                    var repeatition = rows[0].repeatition + 1;
                    var currentId = rows[0].id;
                    var query = conn.query("UPDATE nutritions_process SET repeatition = ?, updated_at = ? WHERE id = ?", [repeatition, data.updated_at, currentId], function(error, rows) {
                        console.log(query.sql);
                        if(error) {
                            console.log('query error', error);
                            reject(error);
                        } else {
                            console.log('update nutrition', rows);
                            resolve(rows);
                        }
                    });
                }
            });

        });

    }
}

class Nutritions {
//////////////////////////////////////////////////////////////////
    static getNutritions(conn, next) {
        return new Promise(function (resolve, reject) {
            var query = conn.query("SELECT breakfast_table.id as pos, breakfast_table.*, morning_snack_table.morning_snack, morning_snack_table.morning_snack_image, morning_snack_table.morning_snack_detail, lunch_table.lunch, lunch_table.lunch_image, lunch_table.lunch_detail, afternoon_snack_table.afternoon_snack, afternoon_snack_table.afternoon_snack_image, afternoon_snack_table.afternoon_snack_detail, dinner_table.dinner, dinner_table.dinner_image, dinner_table.dinner_detail FROM (SELECT nutritions.id as id, nutritions.nutrition_day as nutrition_day, recipes.recipe_name as breakfast, recipes.image_link as breakfast_image, recipes.detail as breakfast_detail	FROM nutritions LEFT JOIN recipes ON nutritions.breakfast = recipes.id) as breakfast_table LEFT JOIN (SELECT nutritions.nutrition_day as nutrition_day, recipes.recipe_name as morning_snack, recipes.image_link as morning_snack_image, recipes.detail as morning_snack_detail	FROM nutritions LEFT JOIN recipes ON nutritions.morning_snack = recipes.id) as morning_snack_table ON breakfast_table.nutrition_day = morning_snack_table.nutrition_day LEFT JOIN (SELECT nutritions.nutrition_day as nutrition_day, recipes.recipe_name as lunch, recipes.image_link as lunch_image, recipes.detail as lunch_detail FROM nutritions LEFT JOIN recipes ON nutritions.lunch = recipes.id) as lunch_table ON breakfast_table.nutrition_day = lunch_table.nutrition_day LEFT JOIN (SELECT nutritions.nutrition_day as nutrition_day, recipes.recipe_name as afternoon_snack, recipes.image_link as afternoon_snack_image, recipes.detail as afternoon_snack_detail FROM nutritions LEFT JOIN recipes ON nutritions.afternoon_snack = recipes.id) as afternoon_snack_table ON breakfast_table.nutrition_day = afternoon_snack_table.nutrition_day LEFT JOIN (SELECT nutritions.nutrition_day as nutrition_day, recipes.recipe_name as dinner, recipes.image_link as dinner_image, recipes.detail as dinner_detail FROM nutritions LEFT JOIN recipes ON nutritions.dinner = recipes.id) as dinner_table ON breakfast_table.nutrition_day = dinner_table.nutrition_day WHERE breakfast_table.id BETWEEN 1 and 7", function (error, rows) {
            
                if(error) {
                    console.log('query error: ', error);
                    reject('Check your query');
                } else if(rows.length < 1) {
                    reject('no such data');
                } else {
                    resolve(rows);
                }
            });

        });
    }
    
}

//start Server
var server = app.listen(3000,function(){

   console.log("Listening to port %s",server.address().port);

});
